:PROPERTIES:
:ID:       2FA973D6-E2CC-4D28-9386-DF8A97734B88
:END:
#+title: spring-break-group-project-work

* TODO Functional requirements
- Records Management Systems
  1. Access level - list all operations on the RMS and access levels for all user groups
  like chief admin having all the privileges and lecturers have least access and modification
  rights. All the fields color-coded, green, amber, red...
  Break it down to sub-groups like student records, staff records, modules etc...

  2. Entity tables - address, student, tutor, staff, module, course, attendance, grade...
     Make ERM table like in DB classes, types of data, attributes etc...
     Each entity has own table.(see Claybrook Zoo example).

     This table shows level of access/permission in RMS. This should be shown for every entity.

    | Operation     | Chief Admin | Admin | Tutor |
    |---------------+-------------+-------+-------|
    | Create Record |             |       |       |
    | Amend Record  |             |       |       |
    | Assign        |             |       |       |
    | Display       |             |       |       |
    | Search        |             |       |       |
    | Delete        |             |       |       |
    | Archive       |             |       |       |

* TODO System Interface Designs - preparation

** Go through the files for the project and get the specifications

** First watch the videos of the software and take notes and sketches of first draft

** Look up how the document is structured, is it only wire-frames or is there a text too

** Prepare 3 (three) versions of the landing page

** DONE Draft Interface Designs (Records Management Systems - Wire-frame - Figma)
CLOSED: [2022-03-30 Wed 20:18] SCHEDULED: <2022-03-29 Tue 14:00>
- Are we doing system navigation diagram, system screen mock-ups and system actiivity event diagrams???
  points 4.1.2, 4.1.3, 4.1.4 - NEED ANSWERS
- Start with designing the login page
- Main (landing page) - three versions, one with side menu, one with tabs on the top and
  one with tabs as in the Classe365
- Follow with wire-frames of 5 - 7 pages
- Maybe add room booking and tutor appointments pages???
- CHANGE COPYRIGHT TO COPYLEFT!!!
