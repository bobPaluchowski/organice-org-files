#+title:      Setting-up-Organice
#+date:       [2023-12-20 Wed 22:20]
#+filetags:   :general:
#+identifier: 20231220T222057

* TODO Steps to setup Organice

**  Follow Bard's instructions.
- [ ] - Create GitLab repository with my Org files in "notes" directory.
- [ ] - Change the repository path in Organice on my iPhone.
- [ ] - Change GitLab password for something easier to memorize.
- [ ] - Side note: setup OBS for streaming.
- [ ] - Side note: setup OBS for streaming.
- [ ] - Consolidate RoamNotes, org, Documents/notes -> change paths in init.el.
